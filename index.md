---
layout: page
---

Hi, I'm Will Da Silva. I'm a software developer by trade (currently working on [Meltano](https://meltano.com/)), and I enjoy discussing and debating philosophical and political topics. [I write about some of these topics here, among other things](/posts/).

My interests include:
- Neuroscience - I'd like to work on biotech for the brain someday.
- Climbing - I try to climb at least one per week, usually bouldering, and can solve grade V7 problems occasionally.
- The immune system
- Urbanism
- Camping
- Cooking
- Baking
- Artificial intelligence
- Kayaking
- Animation - I consider it to be a superior medium to live-action video in most respects.
- Type theory
- Biking
- Media analysis - [TV Tropes](https://tvtropes.org/) is one of my favourite websites; I love finishing a good series, and then diving into the TV Tropes pages for it.
- Philosophy - I plan to eventually have enough written here to provide an overview of my philosophical views.
- Long fantasy series with rich worldbuilding
