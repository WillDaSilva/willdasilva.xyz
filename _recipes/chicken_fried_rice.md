---
layout: recipe
title: Chicken Fried Rice
date: 2021-08-18
---

This recipe uses bone-in skin-on chicken thighs, but can easily be adapted to work with just about any kind of chicken. I just find it works best with the thighs. If the chicken you're using has no skin, or just isn't as fatty as the thighs are, you'll have to add additional oil.

Ingredients:
------------
- 4 bone-in skin-on chicken thighs
- 1 tbs ginger
- 2 large cloves garlic
- 1.5 large yellow onions
- 2 cups leftover rice
- 4 large eggs

Instructions:
-------------

1. Oil a cast iron skillet. Set over medium heat. Season the chicken thighs all over with salt and pepper. Cook skin-side down without moving until the fat has rendered and the skin is golden-brown and crispy, 15-25 minutes.
2. Flip thighs, and cook until done, 15-25 minutes. Transfer thighs to plate.
3. Dice the onion, then fry them in the residual chicken fat on high heat for 5-10 minutes, until softened and starting to brown.
4. Microplane the ginger, crush the garlic, and add both to the pan. Stir in, and cook for ~1 minute.
5. Mix in rice, and soy sauce. Turn off the heat.
6. Make scrambled eggs (with decently large chunks). Mix them into the fried rice until just combined.
7. Plate up rice, and nestle a chicken thigh on top. Drizzle oyster sauce over-top of the rice. Serve immediately.
