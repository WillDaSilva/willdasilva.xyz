---
layout: recipe
title: Beef Cabbage Stir Fry
date: 2021-08-14
---

Ingredients:
------------
- 1/2 head green cabbage
- 500 g ground beef
- bacon
- Worcestershire sauce
- soy sauce
- sriracha sauce
- sesame oil
- brown sugar
- yellow onions
- cooked rice
- green onions


Instructions:
-------------

1. Cut and caramelize onions in large skillet. Finish with Worcestershire sauce.
2. Slice cabbage into thin strips.
3. Add cabbage to the skillet. Cook for a while, then add beef. Cook until beef is done.
4. Add soy sauce, sesame oil, sriracha sauce, and brown sugar to the skillet.
5. Cook bacon in a separate skillet. Once done, turn off the heat and cut in the green onions to fry in the bacon fat. Cut the bacon into small pieces, then dump the bacon skillet into the beef & cabbage skillet.
6. Top with sesame seeds and sriracha sauce.
7. Serve over rice. 
