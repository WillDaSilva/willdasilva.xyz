---
layout: recipe
title: Carrot Lentil Stuff
date: 2023-12-14
---

Ingredients:
------------
- carrots
- celery (optional)
- onions (optional)
- baking soda
- red lentils
- ground black pepper
- turmeric
- tamarind sauce
- fresh Thai red chilies

Instructions:
-------------

1. Cut carrots, celery, and onions down to your desired size.
2. Fry in a stainless steel skillet with butter. Add a light sprinkle of baking soda (be conservative - a little goes a long way). Cook, stirring occasionally, until there is lots of brown stuff on the pan.
4. Mix in red lentils, and let them get a bit toasted.
5. Deglaze with soy sauce, then add enough water to cover.
6. Cook until the water is mostly gone. Add more water and cook down again until the desired tenderness is achieved.
7. Turn off the heat, and mix in black pepper, garam masala, turmeric, and tamarind sauce.
8. Slice the fresh Thai red chilies, and mix them in.
9. Serve warm, ideally over toast/rice/naan.
