---
layout: recipe
title: Musakhan Rolls
date: 2024-04-28
---

Ingredients:
------------
- Chicken:
  - 1 kg skinless chicken breast
  - 30 mL extra virgin olive oil
  - 4 cloves minced garlic
  - 2 tsp fine salt
  - 1 tsp ground black pepper
  - 1/2 tsp ground cinnamon
  - 1/2 tsp ground cardamon
- Onions:
  - 8 medium onions
  - 1 tsp fine salt
  - 30 mL extra virgin olive oil
  - 3/4 tsp ground black pepper
  - 3 tbsp ground sumac
- Musakhan:
  - prepared chicken (see above)
  - prepared onions (see above)
  - 3 tbsp ground sumac
  - 70 mL lemon juice
  - 10 g pomegranate molasses
- Rolls:
  - prepared musakhan (see above)
  - 4 large saj or markouk bread
  - 10 g flour
  - 10 g water
  - 30 mL extra virgin olive oil
  - 1 tsp ground sumac

Instructions:
-------------

1. Butterfly the chicken breasts or otherwise cut/make them into equally thick pieces that will cook evenly in a flat layer in a skillet. The chicken will be shredded later, so don't worry about making it pretty.
2. Mix the olive oil, minced garlic, salt, ground black pepper, ground cinnamon, and ground cardamon together in a large bowl.
3. Add the chicken to the bowl, and mix to ensure every piece of chicken is coated in the spice mixture. Wait 30 minutes before cooking the chicken. While waiting, prepare the onions as described below.
4. Cut each onion into thin strips. This can be done quickly by removing the skin and/or outermost layer, then cutting each onion in half through the stem and root, then making transverse cuts from the stem to the root.
5. Cook the chicken in batches in a stainless steel or carbon steel skillet over medium heat. For each batch, lay the chicken in the skillet in a single layer. Avoid overcrowding the skillet. After ~3 minutes, flip the chicken. Add water to the pan, and scrap up the browned stuff. Cover the skillet with a lid and cook for an additional ~3 minutes. Use a thermometer to know when the chicken is done cooking if you're uncertain. Focus on ensuring a nice layer of browned stuff builds up on the bottom of the skillet, but does not burn. Add water as necessary to prevent burning on the bottom of the pan. Set aside each piece of cooked chicken.
6. The skillet should now have a nice layer of browned stuff. Add the olive oil into the skillet along with the sliced onions, and scrape the browned stuff off with the softening onions and a wooden utensil. Cover the onions and let them cook, stirring occasionally, for 20 minutes.
7. Uncover the onions, and add the ground black pepper and ground sumac. Mix to combine, and then cook for an additional 10-20 minutes, until your desired level of caramelization and jamminess has been achieved.
8. Roughly shred the chicken, then mix it with the onions, and add the additional sumac, the lemon juice, and the pomegranate molasses. Take it off of the heat. This is musakhan, and it can be used in a variety of recipes. If served on its own, whole chicken thighs/pieces are generally used instead of shredded chicken.
9. Heat the oven to 190°C if you plan to bake the rolls immediate after forming them. Altneratively, after forming them, they can be frozen, and baked from frozen later.
10. Cut each of the 4 large saj or markouk flatbreads into 8 wedge-shaped slices. Scissors can help with this. If there is a thick ring of bread around the edge, after slicing the bread into wedges, cut off the thick edge.
11. Mix the water and flour together to form a glue.
12. With each slice of bread, add 1/32 of the musakhan to the side opposite the point, then fold in the sides, and roll the bread up towards the point. Apply the water + flour glue to the tip of the roll before you finish rolling it to help it remain sealed after baking. Add each finished roll to a baking sheet.
13. Brush the rolls with olive oil, then bake for 20-25 minutes. Sprinkle ground sumac on top after removing them from the oven.
14. Let cool for a few minutes, then serve and enjoy with [a delicious tangy yogurt dip](/recipes/tangy-yogurt-dip)
