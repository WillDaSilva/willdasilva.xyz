---
layout: recipe
title: Carrot Cake
date: 2021-08-14
---

Makes one 9x13 inch cake, or two 9 inch round cakes (which I recommend layering).

Ingredients:
------------

Cake:
- Dry:
  - 250 g flour
  - 1/2 tsp salt
  - 1 tsp baking soda
  - 2 tsp baking powder
  - 1/2 tsp ground ginger
  - 1/4 tsp ground nutmeg
  - 1/2 tbsp ground cinnamon
- Wet:
  - 4 eggs
  - 180 mL canola oil
  - 100 g sugar
  - 300 g brown sugar
  - 1 tsp pure vanilla extract
  - 300 g grated carrots

Frosting:
- 250 g cream cheese, softened to room temperature
- 125 g butter, softened to room temperature
- 250 g powdered sugar
- 1 tsp pure vanilla extract


Instructions:
-------------

1. Heat oven to 180°C.
2. Mix dry cake ingredients in one bowl, and wet ingredients in another. Then pour the mixed dry ingredients into the bowl with the wet ingredients, and fold to combine.
3. Pour the batter into the pans, then put the pans into the heated oven for 30-35 minutes. Insert a toothpick into the middle of a cake, and ensure it comes out clean to verify that the cake is done baking. Transfer the cakes to cooling racks, and let cool to room temperature.
4. In another bowl, beat cream cheese and butter until smooth. Add powdered sugar and vanilla, then mix until fully combined. Feel free to adjust the amounts of the ingredients used for the frosting either to change the consistency/flavour, or to simply increase the amount if you like your cakes with a generous amount of frosting.
5. Assemble the cakes with the frosting. Refrigerate leftover portions, or the entire cake if it will not be eaten soon.
