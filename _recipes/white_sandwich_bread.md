---
layout: recipe
title: White Sandwich Bread
date: 2021-08-14
---

For almost 5 years (as of the date I write this recipe) I've been baking most of the bread I eat, and I've been making this recipe (or some close variation of it) often. Sometimes as often as every second day, when it's in high demand in my house. It's my go-to bread for sandwiches and toast. Thick slices of it, fresh, and lightly toasted is the basis of many comforting meals I enjoy. But it's also strong enough to slice thin, maybe get a bit wet from sandwich ingredients, and still hold together reliably. It's versatile, very cheap, and easy to make. 

For this recipe I use a pan that's somewhat difficult to obtain outside of Japan - I ended up ordering mine from Japan. It is a 25x12x12 cm (LxWxH) pan with a lid that slides shut across the top. These pans are sometimes referred to as "pullman pans" or "shokupan pans". You can find some items under those names outside of Japan, but usually they're smaller than the pan I use, and it's hardly the largest optional commonly found in Japan. Feel free to use regular loaf pans instead, but without the square-shaped covered pan you won't get those big square slices that I love for sandwiches and toast.


Ingredients:
------------
- 420 g water
- ~660 g flour
- 10 g dry yeast
- 40 g sugar
- 10 g salt
- 45 g butter


Instructions:
-------------

1. In a pot or pan, ideally non-stick (especially if using a large amount of lour), whisk *up to* 160 g of flour into 300 g of water. Start slowly, only adding more flour once what has already been added has no remaining clumps. Heat this mixture on a stove, mixing frequently to heat it evenly and prevent burning, until it reaches 65°C, by which point it will have thickened considerably. This gelatinized mixture is known as a water roux, or tangzhong. It is traditionally made with a 1:5 ratio of flour to water, but here we're trying to pre-cook/hydrate/gelatinize as much flour as possible to get the most out of this technique. Using a water roux for bread allows the flour to hold additional moisture, resulting in a softer and fluffier texture, and bread that takes longer to go stale. The increased softness is partially counteracted by the long kneading time we will use in this recipe, making the bread strong, and suitable for sandwiches.
2. In the bowl of a stand mixer add the remaining 120 g of water, then mix in the yeast, and a few grams of sugar. Add 350 grams of flour (to start), then the salt, butter, and water roux. Roughly mix the ingredients together into a shaggy dough, and then knead with a dough hook in a stand mixer on low speed for 10 minutes. Add additional flour and scrape the sides of the bowl as needed. After 10 minutes you should be left with a moist dough that's slightly hard to work with, and has well developed gluten.
3. Pull the dough out of the stand mixer bowl, and then fold it into itself on the bottom, rolling and pinching it shut to form a ball. Drop this ball into the mixing bowl, or some other bowl where it can rise covered by a towel or plate until doubled in size, ~1 hour.
4. Heat the oven to 225°C.
5. Use a silicone spatula to cost the inside (including the lid) of the loaf pan with butter.
6. Gently deflate the dough, and shape it from the ball into an evenly proportioned ovoid. Drop it into the loaf pan, then slide the lid shut. Let rise for ~40 minutes, until the dough almost reaches the lid.
7. Put the pan into the oven. Lower the oven's temperature to 200°C, and then bake for 36 minutes.
8. Remove the pan from the oven, remove the lid from the pan, and then invert the pan onto a cooling rack. Let the bread cool to room temperature before slicing.
