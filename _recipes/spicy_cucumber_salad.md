---
layout: recipe
title: Spicy Cucumber Salad
date: 2023-12-19
---

Ingredients:
------------
- English cucumber
- rice vinegar
- white sugar (optional)
- roasted peanuts
- black sesame seeds
- fresh Thai red chilies

Instructions:
-------------

1. Use a vegetable peeler to cut thin strips of cucumber lengthwise. The result should be similar in form to long flat noodles of varying widthes. Stack them on top of each other as you slice them, then cut them in half to shorten them to a more manageable length.
2. Add the cucumbers to a jar, and pour enough rice vinegar overtop to just cover. To avoid overusing rice vinegar, agitate and gently press the cucumbers down with a spoon. Optionally, mix in some white sugar to the vinegar to give the final dish some extra sweetness.
3. Close the jar, and place it in the fridge. Wait 1 hour. While waiting, the following 2 steps can be performed.
4. Chop or crush the peanuts until they have reached a consistency good for sprinkling on top of the salad. A small food processor, or mortar and pestle, can help with this.
5. Thinly slice the fresh Thai red chilies crosswise, optionally disgarding the pith and seeds to reduce spciness.
6. After having waited 1 hour, drain the liquid from the cucumbers, then add the cucumbers to the serving bowls. Mix them to avoid having them be stuck against each other in flat layers.
7. Sprinkle the chpped/crushed peanuts, sliced red chilies, and black sesame seeds over the cucumbers. Serve immediately, or after waiting a few minutes for the flavours to intermix slightly.
