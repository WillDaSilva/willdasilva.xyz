---
layout: recipe
title: Tangy Yogurt Dip
date: 2024-04-28
---

Ingredients:
------------
- 100 g strained yogurt
- 10 g tahini
- juice of 1 lime
- 1/4 tsp fine salt
- ground cayenne pepper (to taste)
- water (optional)

Instructions:
-------------

1. Mix the ingredients together, optionally using water to thin the sauce to the desired consistency.
