---
layout: recipe
title: Bagels
date: 2021-09-08
---

Ingredients:
------------
- 500 g water
- 12 g dry yeast
- 25 g sugar
- 800 g flour
- 28 g (~2 tbsp) canola oil
- 18 g salt
- 16 g dry gluten (or extract it from flour manually - see below)
- 15 g (~1 tbsp) baking soda
- honey (to taste)
- 1 egg's white
- Toppings (e.g. sesame seeds, poppy seeds, dry minced garlic, dry minced onion, etc.)

Instructions:
-------------

1. In the bowl of a stand mixer add and whisk the water, yeast, and sugar.
2. If you don't have dry gluten, you can make it yourself with flour and water. I'd recommend watching videos for instructions on how to do this. Essentially you need to mix some flour and water to make a dough, knead it thoroughly, let it rest for 10 minutes, and then wash it in a bowl repeatedly with water until you're left with a ball of gluten. Don't worry too much about getting the perfect amount.
3. Add the flour, then the oil, salt, and gluten. Knead with a dough hook in a stand mixer on low speed for 10 minutes. Add additional flour until the dough pulls away from the sides of the bowl
4. Pull the dough out of the stand mixer bowl, and then fold it into itself on the bottom, rolling and pinching it shut to form a ball. Drop this ball into the mixing bowl, or some other bowl where it can rise covered by a towel or plate until doubled in size, ~1 hour.
5. Heat the oven to 220°C.
6. Prepare a half sheet pan with a sheet of parchment paper over it.
7. Divide the dough into 4 equally sized pieces, and then divide each of those into 3 equally sized pieces, for a total of 12 dough balls. Poke a hole through each, and then stretch it larger and shape the dough into a ring shape. Make the hole larger than you want it to be in the end as it will shrink during proofing and baking. Feel free to twist half of it around through the hole to give the bagel an improved/different texture/shape. Place these on the pan, then let them proof for ~20 minutes.
8. Bring a large pot of water to a boil. Add the honey and baking soda.
9. Using a skimmer lower bagels into the water (as many as can fit in one layer at the surface, probably 3 or 4 at a time). After ~30 seconds, flip the bagels, then continue to boil them for ~90 seconds. Transfer the boiled bagels to a rack on which it can dry, and then back onto the sheet pan.
10. Brush the bagels with egg white, then sprinkle on your preferred toppings.
11. Bake until golden brown, ~18 minutes. Let the bagels cool on a rack.
