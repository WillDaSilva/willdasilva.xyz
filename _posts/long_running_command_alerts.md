---
title: Long-Running Command Alerts
subtitle: How I alert myself that a long-running command has finished
date: 2021-06-14
---

If you're a developer or power-user, then you probably have to run commands that take a reasonably long time to complete fairly frequently. Too long to comfortably stare at your screen waiting for it to complete in any case. I like to be able to be alerted to the fact that one of these long-running commands has finished, so that I can do something else while it's running, but then get right back to it once it's done. In this post I outline the two ways I do this: aurally, and visually (via desktop notifications).

Everything in this post runs on Linux - I don't believe the approaches outlined here would work for macOS, Windows, or most other operating systems. At the time of writing this I'm uses these commands on Linux Mint with Bash as my shell.

Aurally
-------

I define a shell function `tone` like so:

```sh
tone() {
    timeout -k ${2} ${2} speaker-test --frequency ${1} --test sine;
    return 0
} > /dev/null 2>&1
```

It takes 2 arguments: frequency, and duration. It uses the command `speaker-test` provided by ALSA, which plays a sine wave with the specified frequency until the process is killed. To kill the process so that we aren't subjected to an endless pure tone we use the command `timeout` provided by the GNU Core Utilities. We use `> /dev/null 2>&1` to discard the text output.

I then define an alias named `beep`:

```sh
alias beep='tone 523.25 0.35'
```

When run, this plays a nice C<sub>5</sub> for 0.35 seconds. Feel free to experiment with different frequencies/notes, and durations. Note that `speaker-test` will only play sine waves with a frequency between 30 Hz and 8000 Hz by default, but you probably want to stay without that range anyway.



<style>
.wrap-collabsible {
  margin-bottom: 1.2rem 0;
}

input[type='checkbox'] {
  display: none;
}

.toggle-label {
  display: block;
  text-align: center;
  padding: 1rem;
  cursor: pointer;
  border-radius: 2px;
}

.toggle-label::before {
  content: ' ';
  display: inline-block;
  border-top: 5px solid transparent;
  border-bottom: 5px solid transparent;
  border-left: 5px solid currentColor;
  vertical-align: middle;
  margin-right: .7rem;
  transform: translateY(-2px);
}

.toggle:checked + .toggle-label::before {
  transform: rotate(90deg) translateX(-3px);
}

.collapsible-content {
  max-height: 0px;
  overflow: hidden;
}

.toggle:checked + .toggle-label + .collapsible-content {
  max-height: 10000vh;
}

.toggle:checked + .toggle-label {
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}

table {
  margin: auto;
  width: 100%;
  width: -moz-available;
  width: -webkit-fill-available;
  width: fill-available;
  width: stretch;
  text-align: center;
}
</style>

<div class="wrap-collabsible">
  <input id="collapsible" class="toggle" type="checkbox">
  <label for="collapsible" class="toggle-label">Notes to Frequency Table</label>
<div class="collapsible-content">
<table><thead><tr><th>Note</th><th>Frequency (Hz)</th></tr></thead><tbody><tr><td>C<sub>0</sub></td><td>16.35</td></tr><tr><td>C<sup>#</sup><sub>0</sub>/D<sup>b</sup><sub>0</sub></td><td>17.32</td></tr><tr><td>D<sub>0</sub></td><td>18.35</td></tr><tr><td>D<sup>#</sup><sub>0</sub>/E<sup>b</sup><sub>0</sub></td><td>19.45</td></tr><tr><td>E<sub>0</sub></td><td>20.60</td></tr><tr><td>F<sub>0</sub></td><td>21.83</td></tr><tr><td>F<sup>#</sup><sub>0</sub>/G<sup>b</sup><sub>0</sub></td><td>23.12</td></tr><tr><td>G<sub>0</sub></td><td>24.50</td></tr><tr><td>G<sup>#</sup><sub>0</sub>/A<sup>b</sup><sub>0</sub></td><td>25.96</td></tr><tr><td>A<sub>0</sub></td><td>27.50</td></tr><tr><td>A<sup>#</sup><sub>0</sub>/B<sup>b</sup><sub>0</sub></td><td>29.14</td></tr><tr><td>B<sub>0</sub></td><td>30.87</td></tr><tr><td>C<sub>1</sub></td><td>32.70</td></tr><tr><td>C<sup>#</sup><sub>1</sub>/D<sup>b</sup><sub>1</sub></td><td>34.65</td></tr><tr><td>D<sub>1</sub></td><td>36.71</td></tr><tr><td>D<sup>#</sup><sub>1</sub>/E<sup>b</sup><sub>1</sub></td><td>38.89</td></tr><tr><td>E<sub>1</sub></td><td>41.20</td></tr><tr><td>F<sub>1</sub></td><td>43.65</td></tr><tr><td>F<sup>#</sup><sub>1</sub>/G<sup>b</sup><sub>1</sub></td><td>46.25</td></tr><tr><td>G<sub>1</sub></td><td>49.00</td></tr><tr><td>G<sup>#</sup><sub>1</sub>/A<sup>b</sup><sub>1</sub></td><td>51.91</td></tr><tr><td>A<sub>1</sub></td><td>55.00</td></tr><tr><td>A<sup>#</sup><sub>1</sub>/B<sup>b</sup><sub>1</sub></td><td>58.27</td></tr><tr><td>B<sub>1</sub></td><td>61.74</td></tr><tr><td>C<sub>2</sub></td><td>65.41</td></tr><tr><td>C<sup>#</sup><sub>2</sub>/D<sup>b</sup><sub>2</sub></td><td>69.30</td></tr><tr><td>D<sub>2</sub></td><td>73.42</td></tr><tr><td>D<sup>#</sup><sub>2</sub>/E<sup>b</sup><sub>2</sub></td><td>77.78</td></tr><tr><td>E<sub>2</sub></td><td>82.41</td></tr><tr><td>F<sub>2</sub></td><td>87.31</td></tr><tr><td>F<sup>#</sup><sub>2</sub>/G<sup>b</sup><sub>2</sub></td><td>92.50</td></tr><tr><td>G<sub>2</sub></td><td>98.00</td></tr><tr><td>G<sup>#</sup><sub>2</sub>/A<sup>b</sup><sub>2</sub></td><td>103.83</td></tr><tr><td>A<sub>2</sub></td><td>110.00</td></tr><tr><td>A<sup>#</sup><sub>2</sub>/B<sup>b</sup><sub>2</sub></td><td>116.54</td></tr><tr><td>B<sub>2</sub></td><td>123.47</td></tr><tr><td>C<sub>3</sub></td><td>130.81</td></tr><tr><td>C<sup>#</sup><sub>3</sub>/D<sup>b</sup><sub>3</sub></td><td>138.59</td></tr><tr><td>D<sub>3</sub></td><td>146.83</td></tr><tr><td>D<sup>#</sup><sub>3</sub>/E<sup>b</sup><sub>3</sub></td><td>155.56</td></tr><tr><td>E<sub>3</sub></td><td>164.81</td></tr><tr><td>F<sub>3</sub></td><td>174.61</td></tr><tr><td>F<sup>#</sup><sub>3</sub>/G<sup>b</sup><sub>3</sub></td><td>185.00</td></tr><tr><td>G<sub>3</sub></td><td>196.00</td></tr><tr><td>G<sup>#</sup><sub>3</sub>/A<sup>b</sup><sub>3</sub></td><td>207.65</td></tr><tr><td>A<sub>3</sub></td><td>220.00</td></tr><tr><td>A<sup>#</sup><sub>3</sub>/B<sup>b</sup><sub>3</sub></td><td>233.08</td></tr><tr><td>B<sub>3</sub></td><td>246.94</td></tr><tr><td>C<sub>4</sub></td><td>261.63</td></tr><tr><td>C<sup>#</sup><sub>4</sub>/D<sup>b</sup><sub>4</sub></td><td>277.18</td></tr><tr><td>D<sub>4</sub></td><td>293.66</td></tr><tr><td>D<sup>#</sup><sub>4</sub>/E<sup>b</sup><sub>4</sub></td><td>311.13</td></tr><tr><td>E<sub>4</sub></td><td>329.63</td></tr><tr><td>F<sub>4</sub></td><td>349.23</td></tr><tr><td>F<sup>#</sup><sub>4</sub>/G<sup>b</sup><sub>4</sub></td><td>369.99</td></tr><tr><td>G<sub>4</sub></td><td>392.00</td></tr><tr><td>G<sup>#</sup><sub>4</sub>/A<sup>b</sup><sub>4</sub></td><td>415.30</td></tr><tr><td>A<sub>4</sub></td><td>440.00</td></tr><tr><td>A<sup>#</sup><sub>4</sub>/B<sup>b</sup><sub>4</sub></td><td>466.16</td></tr><tr><td>B<sub>4</sub></td><td>493.88</td></tr><tr><td>C<sub>5</sub></td><td>523.25</td></tr><tr><td>C<sup>#</sup><sub>5</sub>/D<sup>b</sup><sub>5</sub></td><td>554.37</td></tr><tr><td>D<sub>5</sub></td><td>587.33</td></tr><tr><td>D<sup>#</sup><sub>5</sub>/E<sup>b</sup><sub>5</sub></td><td>622.25</td></tr><tr><td>E<sub>5</sub></td><td>659.25</td></tr><tr><td>F<sub>5</sub></td><td>698.46</td></tr><tr><td>F<sup>#</sup><sub>5</sub>/G<sup>b</sup><sub>5</sub></td><td>739.99</td></tr><tr><td>G<sub>5</sub></td><td>783.99</td></tr><tr><td>G<sup>#</sup><sub>5</sub>/A<sup>b</sup><sub>5</sub></td><td>830.61</td></tr><tr><td>A<sub>5</sub></td><td>880.00</td></tr><tr><td>A<sup>#</sup><sub>5</sub>/B<sup>b</sup><sub>5</sub></td><td>932.33</td></tr><tr><td>B<sub>5</sub></td><td>987.77</td></tr><tr><td>C<sub>6</sub></td><td>1046.50</td></tr><tr><td>C<sup>#</sup><sub>6</sub>/D<sup>b</sup><sub>6</sub></td><td>1108.73</td></tr><tr><td>D<sub>6</sub></td><td>1174.66</td></tr><tr><td>D<sup>#</sup><sub>6</sub>/E<sup>b</sup><sub>6</sub></td><td>1244.51</td></tr><tr><td>E<sub>6</sub></td><td>1318.51</td></tr><tr><td>F<sub>6</sub></td><td>1396.91</td></tr><tr><td>F<sup>#</sup><sub>6</sub>/G<sup>b</sup><sub>6</sub></td><td>1479.98</td></tr><tr><td>G<sub>6</sub></td><td>1567.98</td></tr><tr><td>G<sup>#</sup><sub>6</sub>/A<sup>b</sup><sub>6</sub></td><td>1661.22</td></tr><tr><td>A<sub>6</sub></td><td>1760.00</td></tr><tr><td>A<sup>#</sup><sub>6</sub>/B<sup>b</sup><sub>6</sub></td><td>1864.66</td></tr><tr><td>B<sub>6</sub></td><td>1975.53</td></tr><tr><td>C<sub>7</sub></td><td>2093.00</td></tr><tr><td>C<sup>#</sup><sub>7</sub>/D<sup>b</sup><sub>7</sub></td><td>2217.46</td></tr><tr><td>D<sub>7</sub></td><td>2349.32</td></tr><tr><td>D<sup>#</sup><sub>7</sub>/E<sup>b</sup><sub>7</sub></td><td>2489.02</td></tr><tr><td>E<sub>7</sub></td><td>2637.02</td></tr><tr><td>F<sub>7</sub></td><td>2793.83</td></tr><tr><td>F<sup>#</sup><sub>7</sub>/G<sup>b</sup><sub>7</sub></td><td>2959.96</td></tr><tr><td>G<sub>7</sub></td><td>3135.96</td></tr><tr><td>G<sup>#</sup><sub>7</sub>/A<sup>b</sup><sub>7</sub></td><td>3322.44</td></tr><tr><td>A<sub>7</sub></td><td>3520.00</td></tr><tr><td>A<sup>#</sup><sub>7</sub>/B<sup>b</sup><sub>7</sub></td><td>3729.31</td></tr><tr><td>B<sub>7</sub></td><td>3951.07</td></tr><tr><td>C<sub>8</sub></td><td>4186.01</td></tr><tr><td>C<sup>#</sup><sub>8</sub>/D<sup>b</sup><sub>8</sub></td><td>4434.92</td></tr><tr><td>D<sub>8</sub></td><td>4698.63</td></tr><tr><td>D<sup>#</sup><sub>8</sub>/E<sup>b</sup><sub>8</sub></td><td>4978.03</td></tr><tr><td>E<sub>8</sub></td><td>5274.04</td></tr><tr><td>F<sub>8</sub></td><td>5587.65</td></tr><tr><td>F<sup>#</sup><sub>8</sub>/G<sup>b</sup><sub>8</sub></td><td>5919.91</td></tr><tr><td>G<sub>8</sub></td><td>6271.93</td></tr><tr><td>G<sup>#</sup><sub>8</sub>/A<sup>b</sup><sub>8</sub></td><td>6644.88</td></tr><tr><td>A<sub>8</sub></td><td>7040.00</td></tr><tr><td>A<sup>#</sup><sub>8</sub>/B<sup>b</sup><sub>8</sub></td><td>7458.62</td></tr><tr><td>B<sub>8</sub></td><td>7902.13</td></tr></tbody></table>
</div>
</div>

If you want to have a different tone play for success versus failure, you can do so like so:

```sh
alias beep='tone `[ $? = 0 ] && echo 523.25 || echo 261.63` 0.35'
```

We get the exit code of the last executed command with `$?`, and check if it was 0 (success) with `[ $? = 0 ]`. If it was a success, the frequency argument is set to C<sub>5</sub> (523.25 Hz). If it was a failure (i.e. a non-zero exit code), the frequency argument is set to C<sub>4</sub> (261.63 Hz).

To hear the difference, set the alias, then run the following:

```sh
true; beep
```

```sh
false; beep
```

If you're unfamiliar with the `true` and `false` commands, they're a fun little part of the GNU Core Utilities. Their docs sum them up nicely:

> true - do nothing, successfully

> false - do nothing, unsuccessfully


Visually
--------

Sometimes I'm not wearing my headphones, or otherwise can't hear audio from my computer. Or sometimes I want to run multiple long-running commands, and have them all alert me when they're done, but not be subjected to a cacophony of pure tones. In these cases I use desktop notifications. They don't grab my attention quite as well as audio usually does, but are better than nothing.

For this, Bash comes with the `alert` alias defined like so:

```sh
notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e 's/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//')"
```

Let's unpack this:

- [`notify-send`](https://manpages.ubuntu.com/manpages/focal/man1/notify-send.1.html) is a neat little program that can send desktop notifications from the command line.
- `--urgency=low` means exactly what it says, but as for what it does, that can vary depending on the notification server and desktop environment. The options are documented in [the desktop notification sepc](https://www.galago-project.org/specs/notification/0.9/x320.html). In practice it probably will result in the notification not appearing over full-screened content, and it'll probably be shown after any higher urgency notifications in the queue, if there are any. If a notification cannot be shown immediately, it is not lost - rather it'll either appear later, or will be minimized to a notification tray.
- `-i "$([ $? = 0 ] && echo terminal || echo error)"` sets the icon used in the notification to a terminal icon if the previous command succeeded (i.e. returned an exit code of 0), or an error icon (a red circle with a white horizontal line in it for me) if the previous command failed (i.e. returned a non-zero exit code). This is done just like how the `beep` alias chooses which tone to play depending on the exit code of the previous command, which was explained above.
- `"$(history|tail -n1|sed -e 's/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//')"` forms the title and body of the notification:
  - `history|tail -n1` gets the last command run, which will probably be of the form `<command>; alert`.
  - `sed -e 's/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'` the text of the last command run is piped into `sed`:
    - `s/^\s*[0-9]\+\s*//` removes all whitespace from the front and back, and all digits from the front
    - `s/[;&|]\s*alert$//'\'` removes any `;`, `&`, or `|` characters from the front, and any whitespace from the back, as well as the word `alert`.

With this, we can use it in much the same way as `beep`:

```sh
true; alert
```

```sh
false; alert
```

We can also include a custom message:

```sh
true; alert '`true` exited with exit code 1, unsurprisingly'
```

Concerns About Long-Running Commands
------------------------------------

It's all well and good that we can alert ourselves with these methods that a long-running command has terminated, but there's something to be said for switching tasks while a command runs. It's well known that we humans are rubbish at switching tasks - especially mentally demanding ones. When a command takes a *very* long time to run, this isn't much of a problem, as you can fully switch away from your task involving the command to immerse yourself in something else. It becomes more of an issue when a command takes long enough that just waiting for it to finish while doing nothing else is unpleasant, i.e. long enough to warrant using one of the methods discussed here to alert yourself that it's done so that you can do something else. You don't have enough time with these commands to fully switch over to another demanding task, and what's worse, by switching away from the mental state you were in when you issued the command you'll have a hard time coming back to it. There have been many times where I've had to wait for around 3-10 minutes for some command (.e.g a compilation command) to complete, then switch back to a mental state suitable to work on whatever I was working on before, then run the command again. Repeat all day long. It's exhausting, and difficult to get much good work done like this.

I find that listening to music can help make sitting through the long-running commands easier. By intentionally not engaging in anything else, and keeping my mind from wandering to other topics, I can re-engage with the task at hand much more quickly after the command terminates. What kind of music you use matters, as music does impose a cognitive load. I find that lyricless music has a lesser cognitive load than music with lyrics in a language that I do not understand, and music with lyrics in a language that I do not understand has a lesser cognitive load than music with lyrics in a language I do understand.

An excellent option for maximizing productivity and mental wellbeing is to meditate while the command runs. I do this sometimes, but have trouble bringing myself to do it regularly, even though I've observed it's great at keeping my mind prepared to get back to the task I was just working on, and results in other benefits.

If you are going to do something else with the time taken by the long-running command, try to do something that isn't very mentally demanding or stimulating - or at least uses different parts of the brain. Exercise is a good option, as well as certain monotonous chores, such as washing dishes.

Whatever you do, try to avoid sites like Reddit, Hacker News, Twitter, and anything else that can effectively grab your attention. Phones can be pretty bad about this too, as can message boards, chatrooms, and chatting with friends in-person. Reflect on what has a tendency to grab and hold onto your attention, and then try to avoid all of those things if you want to stay on-task through a long-running command.
