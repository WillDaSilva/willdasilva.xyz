---
title: Forgiveness and Punishment
subtitle: A lightly-edited discussion between myself and an acquaintance over Discord
date: 2020-06-26
---

Note: Prior to posting this discussion, permission was obtained from all who contributed. Some edits have been made to correct typographical mistakes, and to streamline the discussion.

Christopher:
> How should people be forgiven for their mistakes?
<br/><br/>
When and how? If they are forgiven by you, are they waved of punishment?

Will:
> Are we talking forgiveness in a personal context, or in a societal one?
<br/><br/>
The latter is a lot more complicated.
<br/><br/>
A fun related topic is redemption.

Christopher:
> Mostly personal, nothing legal or social. Forgiving a friend who wronged you.
<br/><br/>
I'm not asking for any right or wrong answers, I just want to know how others would go about it

Will:
> Forgiveness isn't something anyone owes to anyone else. If a person is seeking forgiveness, and they're slighted by the fact that the one they're seeking it from isn't giving it, then I don't think they're ready to be forgiven.
<br/><br/>
Personally I think forgiveness is due when the person who seeks forgiveness has shown that they're truly remorseful over whatever bad thing they did, and they've taken (or are taking) steps/precautions to not let that bad thing happen again. They need to admit to themselves and others that they messed up, and they need to work to improve themselves. Sometimes they might also have to make an effort to repair whatever bad thing they did too, if applicable.
<br/><br/>
As for "if they are forgiven, are they waived of punishment": I don't really believe in punishment as a useful thing. I can understand doing things to others that harm them if harming them accomplishes some goal, or otherwise makes the world better, but doing it for emotional reasons is immature.
<br/><br/>
So I would say they're waived of punishment either way.

Christopher:
> That is the same way I approach forgiveness. I'll add that forgiveness is never deserved.
<br/><br/>
I try to forgive those that wrong me as often as possible, without simply handing it out, in the same way the Lord forgives me for my sins when I ask for it.
Punishment probably isn't necessary if the lesson is learned and they're aware of their wrongdoing.

Will:
> But is it necessary even when they didn't learn anything, and aren't aware of their wrongdoing? Punishment may be a side-effect of trying to make them learn/improve, but it should never be the goal.

Christopher:
> Are you saying the goal shouldn't be punishment or the goal shouldn't be to make them learn/improve?

Will:
> The goal shouldn't be punishment.

Christopher:
> Because punishment doesn't have the be prison or anything like that
<br/><br/>
or a negative sanction
<br/><br/>
Well, yes a negative sanction is necessary in order to explain to them what they did was wrong

Will:
> I'll use a simple example to illustrate what I mean: when my cat does something bad, I'll usually spray her with a mist of water (so long as almost no time has passed between her doing the bad thing, and me spraying her - cats have very limited short term memory), which could be seen as punishment. The goal here isn't to punish her, but to create a negative association in her mind about whatever she just did. This discourages her from doing that bad thing again, which is good.
<br/><br/>
So I'm teaching her in a way
<br/><br/>
In a way that looks like punishment, but that's not what I'm trying to do
<br/><br/>
And I think that distinction is important

Christopher:
> You're right, Will, the goal should never be punishment. The goal should be to make them learn and improve.
<br/><br/>
If they ask for forgiveness by understanding that what they did was wrong, the goal has been met.
<br/><br/>
So your cat & water example makes sense, but now I have another question
<br/><br/>
should the goal be to make them learn and improve or get them to stop the bad action?
<br/><br/>
I guess stopping the bad action ties in with "improving", but we would ideally want to teach the cat that doing something bad is a bad thing, rather than doing something bad means receiving punishment.

Will:
> Cats lack that level of abstract reasoning, unfortunately.
<br/><br/>
The negative/positive reinforcement strategies are the only things that works on them because of their limited mental ability.

Christopher:
> Absolutely. Let's ask another question then.
<br/><br/>
Let's retain the premise, but change the subject, this time being a person who is capable of this reasoning, instead of a cat.

Will:
>
> > person who is capable of reasoning
>
> This just got messy.
<br/><br/>
People certainly like to think of themselves are being rational actors, but the truth is that we often aren't. Larger brains are more costly from an evolutionary perspective, so we aren't optimized to only our high brain functions. Instead the brain prefers taking a variety of irrational shortcuts. This is extremely unfortunate, obviously.
<br/><br/>
Even if we could assume that someone is acting fully rationally all of the time, there's still the question of bad actors.
<br/><br/>
People can try to game any system that isn't hardened against bad actors, so if you're trying to improve people you'll need to be clever about it.
<br/><br/>
As for what a bad actor is, usually it's just a person who has different goals than you.
<br/><br/>
So it's kinda arbitrary.
<br/><br/>
This starts to dip into the broader political questions about how we should handle/prevent crime.

Christopher:
> It really does, but my question has definitely been answered, about forgiveness.

Will:
> Actually I'd like to change something I said. A bad actor doesn't necessarily have different goals at all. They may have similar goals, but different underlying values and metaphysical beliefs.
<br/><br/>
But they must have either different goals, or different values/metaphysical beliefs. They cannot be fully rational and have the same goals and have the same values/metaphysical beliefs, unless it is you who is not fully rational
<br/><br/>
I think the worse situation is when two entities share goals, but differ in underlying values/metaphysical beliefs. It's easier to convince someone of your goal being worthy of their support than it is to change core aspects about themselves.

Christopher:
> You've got me wondering on how crime and punishment should be handled now.

Will:
> That's good

Christopher:
> Forgiveness has been answered, and so has what the intended purpose purpose of punishment is supposed to be, but now I would like to know how punishment should be handled.
<br/><br/>
on a personal, social, and crime level
<br/><br/>
not religiously, because those questions are already clearly answered for me
<br/><br/>
but it seems like the punishment here is very similar when it comes to personal social and legal
<br/><br/>
it's some kind of separation

Will:
> Would you not even entertain the idea that you could be wrong about the religious aspect? It's a tremendously interesting topic.
<br/><br/>
I'm actually reading The Book of Job right now, which is kinda on-topic

Christopher:
> Entertain the idea, I suppose, but my faith isn't changing

Will:
> Starting a discussion by accepting a conclusion can only prevents you from improving yourself. Just my two cents.
<br/><br/>
As for how we should handle criminal justice, broadly, I think we need to treat groups of people more like an organism than a collection of individuals.
<br/><br/>
Because that's how groups of people behave
<br/><br/>
An implication of this is the idea that we're all in this together. Instead of fighting each other to secure the next highest spot in a societal hierarchy, we need to try to elevate everyone.
<br/><br/>
A rising tide lifts all boats
<br/><br/>
A corollary of these beliefs is that the focus shouldn't be on treating symptoms (like crimes being committed), but instead treating the underlying causes (lack of opportunity, lack of purpose in life, low education, etc.)
<br/><br/>
Not to say that we should ignore the crimes and such
<br/><br/>
But they're not nearly as important as the underlying issues that lead to them

Christopher:
> I was just about to ask.
<br/><br/>
I agree entirely.

Will:
> It's like if a person was a heavy smoker their entire life, and then blamed a bad cell division for their cancer. Sure the malfunctioning cell did something bad, and it should be treated, but there is a much more pressing underlying issue.
